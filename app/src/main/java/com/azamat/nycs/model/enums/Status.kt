package com.azamat.nycs.model.enums

enum class Status {
    SUCCESS,
    ERROR
}