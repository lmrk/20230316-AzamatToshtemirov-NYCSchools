package com.azamat.nycs.model.enums

enum class ItemClick {
    PARENT,
    PHONE,
    MAP
}