package com.azamat.nycs.base

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BaseActivity<T : ViewDataBinding>() : LoggingActivity() {

    protected lateinit var binding: T
        private set

    abstract fun layoutResId(): Int

    abstract fun viewDidLoad()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutResId())
        binding.lifecycleOwner = this
        viewDidLoad()
    }
}