package com.azamat.nycs.util

import android.content.Context
import android.content.SharedPreferences
import com.azamat.nycs.model.enums.SharedPreferenceKey

object SharedPreferences {

    lateinit var sharedPreferences: SharedPreferences

    fun initSharedPreferences(preferenceName: String, context: Context) {
        sharedPreferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
    }

    fun getString(key: SharedPreferenceKey) = sharedPreferences.getString(key.name, null)
    fun putString(key: SharedPreferenceKey, value: String?) =
        sharedPreferences.edit().putString(key.name, value).apply()
}