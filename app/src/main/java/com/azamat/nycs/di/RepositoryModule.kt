package com.azamat.nycs.di

import com.azamat.nycs.model.repository.remote.RemoteRepository
import com.azamat.nycs.model.repository.remote.RemoteRepositoryImpl
import com.azamat.nycs.model.repository.room.SchoolRoomRepository
import com.azamat.nycs.model.repository.room.SchoolRoomRepositoryImpl
import com.azamat.nycs.model.repository.room.ScoreRoomRepository
import com.azamat.nycs.model.repository.room.ScoreRoomRepositoryImpl
import org.koin.dsl.module


val repositoryModule = module {
    factory<RemoteRepository> { RemoteRepositoryImpl(get()) }
    factory<SchoolRoomRepository> { SchoolRoomRepositoryImpl(get()) }
    factory<ScoreRoomRepository> { ScoreRoomRepositoryImpl(get()) }
}