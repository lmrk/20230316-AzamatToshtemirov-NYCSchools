package com.azamat.nycs.di

import com.azamat.nycs.ui.activity.MainViewModel
import com.azamat.nycs.ui.fragment.schoollist.SchoolsListViewModel
import com.azamat.nycs.ui.fragment.scores.ScoreViewModel
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelsModule = module {
    factory { Dispatchers.IO }
    viewModel { MainViewModel(get(), get(), get()) }
    viewModel { SchoolsListViewModel(get(),get(), get()) }
    viewModel { ScoreViewModel(get(), get(), get()) }
}
